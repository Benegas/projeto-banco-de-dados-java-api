package com.carlosfbbini.projeto_banco_de_dados.Controller;

import com.carlosfbbini.projeto_banco_de_dados.Model.FailedMessages;
import com.carlosfbbini.projeto_banco_de_dados.Service.FailedMessageIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class FailedMessageController {

    @Autowired
    private FailedMessageIService failedMessageIService;


    @GetMapping("/messages/failed")
    public List<FailedMessages> getFailesMessages()
    {
        return this.failedMessageIService.getFailedMessages();
    }

    @PostMapping("/messages/failed/insert")
    public void saveFailedMessage(@RequestBody FailedMessages failedMessage)
    {
        this.failedMessageIService.saveFailedMessage(failedMessage);
    }

    @GetMapping("messages/failed/{id}")
    public FailedMessages getFailedMessageById(@PathVariable("id") Long id) throws Exception
    {
        return this.failedMessageIService.getFailedMessageById(id);
    }
}
