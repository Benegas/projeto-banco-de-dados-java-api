package com.carlosfbbini.projeto_banco_de_dados.Controller;

import com.carlosfbbini.projeto_banco_de_dados.Model.Message;
import com.carlosfbbini.projeto_banco_de_dados.Service.MessageIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class MessageController {
    @Autowired
    private MessageIService messageIService;

    @GetMapping("/messages")
    public List<Message> getMessages()
    {
        return messageIService.findAll();
    }

    @PostMapping("/save/message")
    public void setMessage(@RequestBody Message message)
    {
        messageIService.save(message);
    }

    @PutMapping("/update/{id}")
    public Message updateMessage(@PathVariable("id")Long id, @RequestBody Message message) throws Exception
    {
        return messageIService.update(id, message);
    }

    @PostMapping("/update/sent/{id}")
    public Message updateSent(@PathVariable("id")Long id) throws Exception
    {
        return messageIService.updateSent(id);
    }
}
