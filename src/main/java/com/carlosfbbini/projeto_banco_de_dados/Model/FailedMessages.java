package com.carlosfbbini.projeto_banco_de_dados.Model;


import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name="failed_message")
public class FailedMessages {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY) // Lazy não carrega tudo.
    @JoinColumn(name = "message")
    private Message message;
    @Column(name = "createdAt", columnDefinition = "TIMESTAMP NOT NULL DEFAULT NOW()")
    private Timestamp createdAt;
    @Column(name = "deletedAt")
    private Timestamp deletedAt;
    @Column(name = "updatedAt", columnDefinition = "TIMESTAMP NOT NULL DEFAULT NOW()")
    private Timestamp updatedAt;

    public FailedMessages(Message message, Timestamp createdAt, Timestamp updatedAt) {
        this.message = message;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public FailedMessages() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp created_at) {
        this.createdAt = created_at;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deleted_at) {
        this.deletedAt = deleted_at;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updated_at) {
        this.updatedAt = updated_at;
    }
}
