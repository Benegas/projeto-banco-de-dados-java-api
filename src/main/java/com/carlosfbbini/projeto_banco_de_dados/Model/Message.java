package com.carlosfbbini.projeto_banco_de_dados.Model;


import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name="message")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name= "message")
    private String message;
    @Column(name= "number")
    private String number;
    @Column(name="name")
    private String name;
    @Column(name = "whatsName")
    private String whatsName;
    @Column(name= "updatedAt", columnDefinition = "timestamp DEFAULT now() not null")
    private Timestamp updatedAt;
    @Column(name= "createdAt", columnDefinition = "timestamp DEFAULT NOW() not null")
    private Timestamp createdAt;
    @Column(name= "deletedAt", columnDefinition = "timestamp default NULL")
    private Timestamp deletedAt;
    @Column(name="send", columnDefinition = "boolean DEFAULT false not null")
    private boolean send;

    public Message()
    {
        super();
    }

    public Message(String message, String number, String name, String whatsName) {
        this.message = message;
        this.number = number;
        this.name = name;
        this.whatsName = whatsName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isSend() {
        return send;
    }

    public void setSend(boolean send) {
        this.send = send;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getWhatsName() {
        return whatsName;
    }

    public void setWhatsName(String whatsName) {
        this.whatsName = whatsName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
