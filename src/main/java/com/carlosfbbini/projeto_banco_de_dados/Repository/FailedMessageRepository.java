package com.carlosfbbini.projeto_banco_de_dados.Repository;

import com.carlosfbbini.projeto_banco_de_dados.Model.FailedMessages;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FailedMessageRepository extends CrudRepository<FailedMessages, Long> {

}
