package com.carlosfbbini.projeto_banco_de_dados.Service;

import com.carlosfbbini.projeto_banco_de_dados.Model.FailedMessages;

import java.util.List;

public interface FailedMessageIService {

    List<FailedMessages> getFailedMessages();
    void saveFailedMessage(FailedMessages failedMessages);
    FailedMessages getFailedMessageById(Long id) throws Exception;
}
