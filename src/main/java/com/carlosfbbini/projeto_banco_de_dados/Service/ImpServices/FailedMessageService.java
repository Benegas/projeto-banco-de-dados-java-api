package com.carlosfbbini.projeto_banco_de_dados.Service.ImpServices;

import com.carlosfbbini.projeto_banco_de_dados.Model.FailedMessages;
import com.carlosfbbini.projeto_banco_de_dados.Model.Message;
import com.carlosfbbini.projeto_banco_de_dados.Repository.FailedMessageRepository;
import com.carlosfbbini.projeto_banco_de_dados.Repository.MessageRepository;
import com.carlosfbbini.projeto_banco_de_dados.Service.FailedMessageIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class FailedMessageService implements FailedMessageIService {

    @Autowired
    private FailedMessageRepository failedMessageRepository;
    @Autowired
    private MessageRepository messageRepository;

    public List<FailedMessages> getFailedMessages()
    {
        return (List<FailedMessages>)  this.failedMessageRepository.findAll();
    }

    public void saveFailedMessage(FailedMessages failedMessage)
    {
        Optional<Message> messageOptional = this.messageRepository.findById(failedMessage.getMessage().getId());
        this.failedMessageRepository.save(this.checkDates(failedMessage));
    }

    public FailedMessages getFailedMessageById(Long id) throws Exception
    {
        Optional<FailedMessages> failedMessagesOptional = this.failedMessageRepository.findById(id);
        return failedMessagesOptional.orElseThrow(() -> new Exception("Messagem não encontrada"));
    }

    // ##################   PRIVATED FUNCTIONS  ##################
    private FailedMessages checkDates(FailedMessages failedMessage)
    {
        if (failedMessage.getCreatedAt() == null) {
            Date date = new Date();
            failedMessage.setCreatedAt(new Timestamp(date.getTime()));
        }
        if (failedMessage.getUpdatedAt() == null){
            Date date = new Date();
            failedMessage.setUpdatedAt(new Timestamp(date.getTime()));
        }
        return failedMessage;
    }
}
