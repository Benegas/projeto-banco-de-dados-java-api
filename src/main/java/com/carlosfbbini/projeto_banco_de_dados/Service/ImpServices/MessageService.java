package com.carlosfbbini.projeto_banco_de_dados.Service.ImpServices;


import com.carlosfbbini.projeto_banco_de_dados.Model.Message;
import com.carlosfbbini.projeto_banco_de_dados.Repository.MessageRepository;
import com.carlosfbbini.projeto_banco_de_dados.Service.MessageIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MessageService implements MessageIService {

    @Autowired
    private MessageRepository messageRepository;

    public List<Message> findAll()
    {
        return (List<Message>) messageRepository.findAll();
    }

    public void save(Message message)
    {

        messageRepository.save(this.checkDates(message));
    }

    public Message update(Long id, Message message) throws Exception
    {
        Optional<Message> optionalMessage = messageRepository.findById(id);
        if (!optionalMessage.isPresent())
            return optionalMessage.orElseThrow(() -> new Exception("Mensagem não encontrada"));
        message.setId(optionalMessage.get().getId());

        return messageRepository.save(this.checkDates(message));
    }

    public Message updateSent(Long id) throws Exception
    {
        Optional<Message> optionalMessage = messageRepository.findById(id);
        if (!optionalMessage.isPresent())
        {
            return optionalMessage.orElseThrow(() -> new Exception("Mensagem não encontrada"));
        }
        optionalMessage.get().setSend(true);
        return messageRepository.save(optionalMessage.get());
    }


// ##################   PRIVATED FUNCTIONS  ##################
    private Message checkDates(Message message)
    {
        if (message.getCreatedAt() == null) {
            Date date = new Date();
            message.setCreatedAt(new Timestamp(date.getTime()));
        }
        if (message.getUpdatedAt() == null){
            Date date = new Date();
            message.setUpdatedAt(new Timestamp(date.getTime()));
        }
        return message;
    }
}
