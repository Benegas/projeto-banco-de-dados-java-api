package com.carlosfbbini.projeto_banco_de_dados.Service;


import com.carlosfbbini.projeto_banco_de_dados.Model.Message;

import java.util.List;

public interface MessageIService {

    List<Message> findAll();
    void save(Message message);
    Message update(Long id, Message message) throws Exception;
    Message updateSent(Long id) throws Exception;
}
